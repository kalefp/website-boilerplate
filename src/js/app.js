// menu
const menuList = document.querySelector(".menu__list");
const menuToggle = document.querySelector(".menu__toggle");

menuToggle.addEventListener("click", () => openMenu(menuList));

function openMenu(menu) {
  menu.classList.toggle("active");
}
