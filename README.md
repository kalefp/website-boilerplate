## Pierwsze uruchomienie

1. Pobieramy repozytorium do siebie.
2. Używamy komendy
   > npm install
3. Tworzymy plik **.gitignore**.
4. W pliku umieszczamy katalog **node_modules**, aby zapobiec przenoszeniu go do zdalnego repozytorium. Tak powinna wyglądać zawartość pliku:
   > .gitignore
   >
   > node_modules
5. Gulp jest skonfigurowany. Jeżeli nie jest zainstalowany globalnie, należy to uczynić za pomocą komendy:
   > npm install -g gulp
6. Będąc w folderze, w którym znajduje się plik **gulpfile.js**, uruchamiamy **wiersz poleceń / PowerShell / GitBash** i wpisujemy:
   > gulp watch
7. Na naszym lokalnym serwerze uruchomi się podgląd strony i wszystkie zmiany będą na bieżąco aktualizowane.
